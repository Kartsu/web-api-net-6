﻿using System;

/// <summary>
/// Summary description for Class1
/// </summary>
public class Address
{
    public string Street { get; set; }
    public string City { get; set; }

    public string ZipCode { get; set; }
}
